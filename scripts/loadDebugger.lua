-- make sure path is set for TKO scripts directory
if not package.path:find("TKO") then
    package.path = package.path..";"..lfs.writedir().."\\Missions\\TKO\\scripts\\;"..";"..lfs.writedir().."\\Missions\\TKO\\scripts\\?.lua;"
end

env.info("attempting to load debugger")

local json = require("dkjson")
local debuggee = require("vscode-debuggee")
local startResult, breakerType = debuggee.start(json)
env.info('debuggee start -> ' .. tostring(startResult) .. ", " .. breakerType)

xpcall(
    function()
        -- Code to actually run
        local a = 1 + nil
    end,
    function(e)
        if debuggee.enterDebugLoop(1, e) then
            -- ok
        else
            -- If the debugger is not attached, enter here.
            print(e)
            print(debug.traceback())
        end
    end)

-- poll debugger
function PollDebugger()
    timer.scheduleFunction(PollDebugger, {}, timer.getTime() + 0.01)	--reschedule first in case of Lua error
    debuggee.poll()
end

timer.scheduleFunction(PollDebugger, {}, timer.getTime() + 0.01)	--reschedule first in case of Lua error

local useOnlyBlue = true    -- if you only define convoys once, set this true, blue convoys will be duplicated for red

local ME_GroupData = {}
local savegameFileName = lfs.writedir() .. "Missions\\TKO\\ko_Savegame.lua"
--local savegameFileName = lfs.writedir() .. "Missions\\The Kaukasus Offensive\\extractorTemplate.lua"
local zoneUnitData = {}

local function outText(text)
	--trigger.action.outText(text, 10, false)
	env.info("ME_GroupExtractor: "..text)
end

local function TableSerialization(t, i)													--function to turn a table into a string (works to transmutate strings, numbers and sub-tables)
	if not i then
		i = 0
	end
	if not t then 
		return "nil"
	end
	if type(t) == "string" then
		return "!String! t =" .. t
	end
	
	local text = "{\n"
	local tab = ""
	for n = 1, i + 1 do																	--controls the indent for the current text line
		tab = tab .. "\t"
	end
	for k,v in pairs(t) do
		if type(k) == "string" then
			text = text .. tab .. "['" .. k .. "']" .. " = "
			if type(v) == "string" then
				text = text .. "'" .. v .. "',\n"
			elseif type(v) == "number" then
				text = text .. v .. ",\n"
			elseif type(v) == "table" then
				text = text .. TableSerialization(v, i + 1)
			elseif type(v) == "boolean" then
				text = text .. tostring(v) .. ",\n"
			end
		elseif type(k) == "number" then
			text = text .. tab .. "[" .. k .. "] = "
			if type(v) == "string" then
				text = text .. "'" .. v .. "',\n"
			elseif type(v) == "number" then
				text = text .. v .. ",\n"
			elseif type(v) == "table" then
				text = text .. TableSerialization(v, i + 1)
			elseif type(v) == "boolean" then
				text = text .. tostring(v) .. ",\n"
			end	
		end
	end
	tab = ""
	for n = 1, i do																		--indent for closing bracket is one less then previous text line
		tab = tab .. "\t"
	end
	if i == 0 then
		text = text .. tab .. "}\n"														--the last bracket should not be followed by an comma
	else
		text = text .. tab .. "},\n"													--all brackets with indent higher than 0 are followed by a comma
	end
	return text
end

local function writeTableToFile(_table, _filename)
	local exportData = "local t = " .. TableSerialization(_table, 0) .. "return t"				--The second argument is the indent for the initial code line (which is zero)
	local exportDir = lfs.writedir() .. "Missions\\TKO\\"
	
	outText("saving table to file: ".._filename)
	
	exportFile = assert(io.open(exportDir .. _filename, "w"))
	exportFile:write(exportData)
	exportFile:close()
end

--function loadMissionData()
	-- load dynamic mission data:
	outText("attempting to load savegame")
	local DataLoader = loadfile(savegameFileName)
	if DataLoader ~= nil then		-- File open?
		outText("Loading from '"..savegameFileName.."' successful")
		MissionData = DataLoader()
	else
		outText("Mission Data not found")
		return false
	end
--end

----------------------------------------------
-- collectUnitDataFromME()
-- 
-- retrieve all groups and units related to Mission-Zones from the Mission Editor and save it into zoneUnitData
----------------------------------------------
--function collectUnitDataFromME()
	outText("populating zoneUnitData")
	for categoryName, categoryTable in pairs(MissionData) do
		if type(categoryTable)=="table" and categoryName ~= "properties" then  -- only searching aerodromes
			for objectiveName, objectiveTable in pairs(categoryTable) do
				zoneUnitData[objectiveName] = {}
				outText("collecting "..objectiveName)
								
				-- get units from ME and loop them
				for unitName,unitTable in pairs(mist.DBs.MEunitsByName) do
					-- check if name matches
					--outText("unitTable.category = "..unitTable.category)
					
					if unitTable.category=='vehicle' and unitTable.skill ~= "Client" then
						-- "Vehicle" no Client
						--outText("unitTable.groupName = "..unitTable.groupName)
						if unitTable.groupName:gsub("-",""):find(objectiveName:gsub("-","")) and unitTable.groupName:find("Convoy") then	
							outText("found Convoy groupName = "..unitTable.groupName)
							
							if not zoneUnitData[objectiveName][unitTable.coalition] then
								zoneUnitData[objectiveName][unitTable.coalition] = {}								
								zoneUnitData[objectiveName][unitTable.coalition]['units'] = {}
								zoneUnitData[objectiveName][unitTable.coalition]['groups'] = {}
							end
							
							table.insert(zoneUnitData[objectiveName][unitTable.coalition]['units'],unitName)
							local gTest = 0
							for i,v in ipairs(zoneUnitData[objectiveName][unitTable.coalition]['groups']) do
								if unitTable.groupName == v then
									gTest=1
								end
							end
							if gTest==0 then
								table.insert(zoneUnitData[objectiveName][unitTable.coalition]['groups'],unitTable.groupName)
							end
						end
					end
				end
			end
		end
	end
	outText("END populating units table")
	
	outText("zoneUnitData = "..TableSerialization(zoneUnitData))
--end


----------------------------------------------
-- retrieveMissionDataToFile(filename)
-- 
-- creates a file containing all spawn-tables to be used with mist.dynAdd() sorted by cat and objective
-- spawnGroup: 
	--[[
	['groupSpawnTable'] = {
		['visible'] = false,
		['groupId'] = 1210,
		['name'] = 'TAW_0xDEADBEEF_2ndAGGs KUB AA System #1210',
		['country'] = 0,
		['hidden'] = false,
		['category'] = 2,
		['task'] = {
		},
		['units'] = {
			[1] = {
				['type'] = 'Kub 1S91 str',
				['name'] = 'TAW_0xDEADBEEF_2ndAGGs Kub 1S91 str #2772',
				['unitId'] = 2772,
				['heading'] = 120,
				['playerCanDrive'] = true,
				['skill'] = 'Excellent',
				['x'] = -170494.796875,
				['y'] = 670158.625,
			},
		},
	},
	--]]
----------------------------------------------
--function retrieveMissionDataToFile()
	outText("collecting units")
	
	-- loop through all CATEGORIES in the MissionData
	for category, categoryTable in pairs(MissionData) do 
		-- loop through all OBJECTIVES
		if type(categoryTable)=="table" and category ~= "properties" then 
			outText("harvesting "..category)
			
			--if not ME_GroupData[category] then ME_GroupData[category] = {} end	
			for objective, objectiveProperties in pairs(categoryTable) do
				outText("collecting units in "..objective)
				--if not ME_GroupData[category][objective] then ME_GroupData[category][objective] = {} end
				
					
				-- one coalition after the other
				local coalitionTable = { [1] = 'red', [2] = 'blue', }								
				for i, coalition in pairs(coalitionTable) do
					-- loop through groups
					if zoneUnitData[objective] and zoneUnitData[objective][coalition] then
                        ME_GroupData["properties"] = ME_GroupData["properties"] or {}
                        ME_GroupData["properties"]["convoys"] = ME_GroupData["properties"]["convoys"] or {}
						ME_GroupData["properties"]["convoys"][coalition] = ME_GroupData["properties"]["convoys"][coalition] or {}
						for groupType, groupTable in pairs(zoneUnitData[objective][coalition]) do
							if groupType == 'groups' then
								ME_GroupData["properties"]["convoys"][coalition][objective] = ME_GroupData["properties"]["convoys"][coalition][objective] or {}
								
								-- go through the units and spawn them
								for i,groupName in pairs(groupTable) do							
									outText("adding group in " .. objective .. " for " .. coalition)
									--TODO
									
									local group = Group.getByName(groupName)
                                    ME_GroupData["properties"]["convoys"][coalition][objective].country = group:getUnit(1):getCountry()
									
									ME_GroupData["properties"]["convoys"][coalition][objective].units = {}
									
									for i, unit in pairs (group:getUnits()) do
										local point = unit:getPoint()
										local unitpos = unit:getPosition()	
										local heading = math.atan2(unitpos.x.z, unitpos.x.x)
										if heading < 0 then	heading = heading + 2*math.pi end
										ME_GroupData["properties"]["convoys"][coalition][objective]['units'][i] = {
											x = point.x,
											y = point.z,
											heading = math.deg(heading),
										}
									end
									
									-- get route:
									ME_GroupData["properties"]["convoys"][coalition][objective].route = mist.getGroupRoute(groupName, true)
								end
							end
						end
					end
				end
			end
		end
    end
    
    -- copy blue to red convoys and adjust country
    if useOnlyBlue then
        local country = 0
        --ME_GroupData["properties"]["convoys"]["red"] = mist.utils.deepCopy(ME_GroupData["properties"]["convoys"]["blue"])
        for objective, properties in pairs( ME_GroupData["properties"]["convoys"]["red"]) do
            properties.country = country
        end
    end
	
	writeTableToFile(ME_GroupData,"convoydata.lua")
--end
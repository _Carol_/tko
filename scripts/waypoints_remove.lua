--[[
    waypoints script for DCS

    This scripts removes all waypoints except of one specified unit

    usage: 
    unsanitize lfs in DCS\Scripts\MissionScripting.lua or saving to file will not work. 
    specify masterRouteGroup as the groupname of the unit with the waypoints
    specify output filename 

    run the script via trigger in your mission. close the mission, make a copy of your mission file, and copy the created "mission" file into the root of your .miz (.miz files are zip files)
--]]

do

    local masterRouteGroup = "Senaki-Kolkhi CAP DAGGER 1-1"  -- the route of this unit is going to be used as the master route for all other units
    local missionFile = "C:\\Users\\deadbeef\\Saved Games\\DCS.openbeta\\Missions\\TKO\\miz\\tko\\fin\\mission" -- once finished, remove '_new.lua' from the file, and put it into the .miz zipfile

    -- function to convert a table to a string
    function serialize(t, i)
        i = i or 0
        if not t then 
            return "nil"
        end
        
        local text = "{\n"
        local tab = ""
        for n = 1, i + 1 do		--controls the indent for the current text line
            tab = tab .. "\t"
        end
        for k,v in pairs(t) do
            if type(k) == "string" then
                text = text .. tab .. '["' .. k .. '"]' .. " = "
                if type(v) == "string" then
                    text = text .. '"' .. v:gsub('"','\\"') .. '",\n'
                elseif type(v) == "number" then
                    text = text .. v .. ",\n"
                elseif type(v) == "table" then
                    text = text .. serialize(v, i + 1)
                elseif type(v) == "boolean" then
                    text = text .. tostring(v) .. ",\n"
                end
            elseif type(k) == "number" then
                text = text .. tab .. "[" .. k .. "] = "
                if type(v) == "string" then
                    text = text .. '"' .. v:gsub('"','\\"') .. '",\n'
                elseif type(v) == "number" then
                    text = text .. v .. ",\n"
                elseif type(v) == "table" then
                    text = text .. serialize(v, i + 1)
                elseif type(v) == "boolean" then
                    text = text .. tostring(v) .. ",\n"
                end	
            end
        end
        tab = ""
        for n = 1, i do																		--indent for closing bracket is one less then previous text line
            tab = tab .. "\t"
        end
        if i == 0 then
            text = text .. tab .. "}\n"														--the last bracket should not be followed by an comma
        else
            text = text .. tab .. "},\n"													--all brackets with indent higher than 0 are followed by a comma
        end
        return text
    end

    -- make 1:1 copy of table instead of reference (= mist.utils.deepCopy)
    function deepCopy(object)
		local lookup_table = {}
		local function _copy(object)
			if type(object) ~= "table" then
				return object
			elseif lookup_table[object] then
				return lookup_table[object]
			end
			local new_table = {}
			lookup_table[object] = new_table
			for index, value in pairs(object) do
				new_table[_copy(index)] = _copy(value)
			end
			return setmetatable(new_table, getmetatable(object))
		end
		return _copy(object)
	end


    
    local masterRoute = nil
    local mission = env.mission -- env.mission contains the mission lua table from inside the .mis zipfile    
    -- now find the masterroute
    env.info("looking for masteroute:")
    for coa, category in pairs(mission.coalition) do
        for i, country in pairs(category.country) do
            if not masterRoute then
                if country.plane then
                    for id, group in pairs(country.plane.group) do
                        local groupName = env.getValueDictByKey(group.name)
                        if groupName ~= masterRouteGroup then
                            env.info("found removable route: Plane "..id..": '"..groupName.."'")
                            env.info("\tRoute has "..#group.route.points.." points, removing all but first")
                            for i=2, #group.route.points do
                                group.route.points[i] = nil
                            end
                        else
                            env.info("skipping masterroute ...")
                        end
                    end
                elseif country.helicopter then
                    for id, group in pairs(country.helicopter.group) do
                        local groupName = env.getValueDictByKey(group.name)
                        if groupName ~= masterRouteGroup then
                            env.info("found removable route: Helicopter "..id..": '"..groupName.."'")
                            env.info("\tRoute has "..#group.route.points.." points, removing all but first")
                            for i=2, #group.route.points do
                                group.route.points[i] = nil
                            end
                        else
                            env.info("skipping masterroute ...")
                        end
                    end
                end
            end
        end
    end


    -- save the new mission file
    env.info("all but Master routes removed! Saving new 'mission' File")

    local exportData = "mission = " .. serialize(mission)				--The second argument is the indent for the initial code line (which is zero)
	
    local exportFile = assert(io.open(missionFile, "w"))
	exportFile:write(exportData)
	exportFile:flush()
	exportFile:close()
	exportFile = nil
end

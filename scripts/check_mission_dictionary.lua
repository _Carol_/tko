--[[
    populate farps

    This scripts adds all units from a template FARP to all other farps

    usage:
    - unsanitize lfs in DCS\Scripts\MissionScripting.lua or saving to file will not work. 
    - specify output filenames 
    - specify the FARP name and how many farps per side and objective you have.
        For instance if you have FARP Beslan which consists of two farp-pads for each side, those would be named "FARP Beslan 1" and FARP Beslan 2", what matters is that they 
        have an index at the end of the name. If you create all farps of the same sceme, you can create a template farpname, like "FARP_Template1blue" and "FARP_Template2blue" (masterFARPname = "FARP_Template"), or
        for ease just "FARP1blue" and "FARP2blue" (masterFARPname = "FARP") plus the same with red at the end. the masterFARPname variable specifies just how those are named.

        Now you create the slots for these FARPs, and also name them "FARP1blue whateverslotname". The FARP1 in the slotname will be replaced with your masterFARPname, the rest of the name
        will be kept. The original template-slots will be removed. (if you want to keep them, comment the line with table.remove)
    - run this script after your mission is loaded and ready. 
    - copy the "mission" file into the root directory of your .miz file
    - extract the dictionary file of your .miz into another directory. and open it in a text editor.
    - paste the content of dictionary.lua created by this script without (!) the {} brackets into the brackets of your dictionary file, and copy it back into the same location
        the dictionary file makes sure you don't see "Dict_Key..."-style names in the slot-selection
    
    run the script via trigger in your mission. close the mission, make a copy of your mission file, and copy the created "mission" file into the root of your .miz (.miz files are zip files)
--]]

do

    local masterFARPname = "FARP"  -- the route of this unit is going to be used as the master route for all other units
    local numFARPS = 2              -- how many farps per coalition and objective are in use, if 2 and masterfarp is FARP, this would result in templating FARP1 and FARP2 to all other farps indext 1 and 2

    local missionFile = "C:\\Users\\deadbeef\\Saved Games\\DCS.openbeta\\Missions\\TKO\\miz\\mission" -- once finished, remove '_new.lua' from the file, and put it into the .miz zipfile
    local dictFile = "C:\\Users\\deadbeef\\Saved Games\\DCS.openbeta\\Missions\\TKO\\miz\\f14dictionary.lua" -- append this table to the dictionary inside your .miz file, do not replace!

    local typeHelper = {
        "helicopter",
        "plane",
    }

    -- function to convert a table to a string
    function serialize(t, i)
        i = i or 0
        if not t then 
            return "nil"
        end
        
        local text = "{\n"
        local tab = ""
        for n = 1, i + 1 do		--controls the indent for the current text line
            tab = tab .. "\t"
        end
        for k,v in pairs(t) do
            if type(k) == "string" then
                text = text .. tab .. '["' .. k .. '"]' .. " = "
                if type(v) == "string" then
                    text = text .. '"' .. v:gsub('"','\\"') .. '",\n'
                elseif type(v) == "number" then
                    text = text .. v .. ",\n"
                elseif type(v) == "table" then
                    text = text .. serialize(v, i + 1)
                elseif type(v) == "boolean" then
                    text = text .. tostring(v) .. ",\n"
                end
            elseif type(k) == "number" then
                text = text .. tab .. "[" .. k .. "] = "
                if type(v) == "string" then
                    text = text .. '"' .. v:gsub('"','\\"') .. '",\n'
                elseif type(v) == "number" then
                    text = text .. v .. ",\n"
                elseif type(v) == "table" then
                    text = text .. serialize(v, i + 1)
                elseif type(v) == "boolean" then
                    text = text .. tostring(v) .. ",\n"
                end	
            end
        end
        tab = ""
        for n = 1, i do																		--indent for closing bracket is one less then previous text line
            tab = tab .. "\t"
        end
        if i == 0 then
            text = text .. tab .. "}\n"														--the last bracket should not be followed by an comma
        else
            text = text .. tab .. "},\n"													--all brackets with indent higher than 0 are followed by a comma
        end
        return text
    end

    -- make 1:1 copy of table instead of reference (= mist.utils.deepCopy)
    function deepCopy(object)
		local lookup_table = {}
		local function _copy(object)
			if type(object) ~= "table" then
				return object
			elseif lookup_table[object] then
				return lookup_table[object]
			end
			local new_table = {}
			lookup_table[object] = new_table
			for index, value in pairs(object) do
				new_table[_copy(index)] = _copy(value)
			end
			return setmetatable(new_table, getmetatable(object))
		end
		return _copy(object)
	end


    --
    ---------------------------------------------------------
    --
    -- fun starts here

    local masterFARPs = {}
    local otherFARPs = {}
    local masterHelis = {}
    local dictionary = {}
    local dictIdx = 13900 -- start dictionary at safe distance

    local mission = deepCopy(env.mission) -- env.mission contains the mission lua table from inside the .mis zipfile    
    local f14Table = {}
        
    env.info("go through all objectives and output them")
    for coa, category in pairs(mission.coalition) do
        for cid, country in pairs(category.country) do
            if country.plane then
                for id, plane in pairs(country.plane.group) do
                    if plane.units[1].type == "F-14B" then
                        
                        local groupName = env.getValueDictByKey(plane.name)
                        local unitName = env.getValueDictByKey(plane.units[1].name)

                        plane.name = "DictKey_GroupName_"..dictIdx
                        dictIdx = dictIdx + 1
                        plane.units[1].name = "DictKey_UnitName_"..dictIdx
                        dictIdx = dictIdx + 1

                        dictionary[plane.name] = groupName    -- groupName and unitName are the same, but need their own dictkey (or not?)
                        dictionary[plane.units[1].name] = unitName

                        env.info("Created new dictName for F14 '"..groupName.."'/'"..plane.name.."'")
                    end
                end
            end
        end
    end
    
    -- important! update max dictId!!
    mission.maxDictId = dictIdx

    -- save the new mission file
    
    local exportData = "mission = " .. serialize(mission)				--The second argument is the indent for the initial code line (which is zero)
    local exportFile = assert(io.open(missionFile, "w"))
	exportFile:write(exportData)
	exportFile:flush()
    exportFile:close()
    
    env.info("f14 table file saved")

    exportData = "dictionary = "..serialize(dictionary)				--The second argument is the indent for the initial code line (which is zero)
    exportFile = assert(io.open(dictFile, "w"))
	exportFile:write(exportData)
	exportFile:flush()
    exportFile:close()
    exportFile = nil    --]]
    
    env.info("f14 dictionary saved")
end
